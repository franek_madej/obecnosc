from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY',
                 default='d&u^gojt76iv-9a&)uthyumu1*htjn)5_7vp%i@#^5e%-3*@io')
DEBUG = env.bool('DJANGO_DEBUG', default=True)
