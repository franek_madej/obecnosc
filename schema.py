import graphene

import playarena_scheduler.schema
from playarena_scheduler.schema import MyMutations


class Query(playarena_scheduler.schema.Query, graphene.ObjectType):
    # This class will inherit from multiple Queries
    pass


schema = graphene.Schema(query=Query, mutation=MyMutations)
