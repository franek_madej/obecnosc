const webpackConfig = require('./webpack.config.js')

module.exports = function (config) {
  config.set({
    // to run in additional browsers:
    // 1. install corresponding karma launcher
    //    http://karma-runner.github.io/0.13/config/browsers.html
    // 2. add it to the `browsers` array below.
    browsers: ['ChromiumHeadless'],
    frameworks: ['mocha', 'sinon-chai', 'phantomjs-shim'],
    files: ['./frontend/test/unit/index.js'],
    exclude: ['./frontend/test/unit/specs/*.js'],
    reporters: ['progress', 'coverage'],
    preprocessors: {
      './frontend/test/unit/index.js': ['webpack', 'sourcemap']
    },
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true
    },
    coverageReporter: {
      dir: './coverage',
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    }
  });
};
