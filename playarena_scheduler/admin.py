from django.contrib import admin

from playarena_scheduler.models import (AdditionalPerk, Availability, Player,
                                        Position, Team, Date, Time)

admin.site.register(Team)
admin.site.register(Position)
admin.site.register(AdditionalPerk)
admin.site.register(Player)
admin.site.register(Availability)
admin.site.register(Date)
admin.site.register(Time)
