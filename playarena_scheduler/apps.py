from django.apps import AppConfig


class PlayarenaSchedulerConfig(AppConfig):
    name = 'playarena_scheduler'
