import datetime

from django.contrib.auth.models import User
from mixer.backend.django import mixer

from .helper import GraphQLTestCase


class TestDailyInputView(GraphQLTestCase):
    def test_daily_input_view_empty(self):
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        date = datetime.datetime.now()
        query = '''
            query testDailyInputView($kitNumber: Int, $date: String) {
                dailyInputView(kitNumber: $kitNumber, date: $date) {
                    available
                    time {
                       time
                    }
                }
            }
        '''
        op_name = 'dailyInputView'
        input_dict = {
            'kitNumber': 29,
            'date': date.__str__()[0:10].replace('-', '.', 2)
        }
        expected = {
            'dailyInputView': []
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)

    def test_daily_input_view_with_two_availabilities(self):
        player = mixer.blend('playarena_scheduler.Player',
                             first_name='Franciszek',
                             last_name='Madej',
                             kit_number=29)
        date = mixer.blend('playarena_scheduler.Date',
                           date=datetime.datetime.now().date)
        time_old = mixer.blend('playarena_scheduler.Time',
                               time=datetime.datetime.now().time)
        time = mixer.blend('playarena_scheduler.Time',
                           time=(datetime.datetime.now()
                                 + datetime.timedelta(hours=1, minutes=30)
                                 ).time)
        mixer.blend('playarena_scheduler.Availability',
                    player=player, date=date, time=time_old, available=True)
        mixer.blend('playarena_scheduler.Availability',
                    player=player, date=date, time=time, available=False)
        query = '''
            query testDailyInputView($kitNumber: Int, $date: String) {
                dailyInputView(kitNumber: $kitNumber, date: $date) {
                    available
                    time {
                       time
                    }
                }
            }
        '''
        op_name = 'dailyInputView'
        input_dict = {
            'kitNumber': 29,
            'date': date.__str__().replace('-', '.', 2)
        }
        expected = {
            'dailyInputView': [
                {
                    'available': True,
                    'time': {
                        'time': time_old.__str__()
                    }
                },
                {
                    'available': False,
                    'time': {
                        'time': time.__str__()
                    }
                }
            ]
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)

    def test_daily_input_with_two_players(self):
        player1 = mixer.blend('playarena_scheduler.Player',
                              first_name='Franciszek',
                              last_name='Madej',
                              kit_number=29)
        player2 = mixer.blend('playarena_scheduler.Player',
                              first_name='Jan',
                              last_name='Kowalski',
                              kit_number=10)
        date = mixer.blend('playarena_scheduler.Date',
                           date=datetime.datetime.now().date)
        time = mixer.blend('playarena_scheduler.Time',
                           date=datetime.datetime.now().time)
        mixer.blend('playarena_scheduler.Availability',
                    player=player1,
                    date=date,
                    time=time,
                    available=True)
        mixer.blend('playarena_scheduler.Availability',
                    player=player2,
                    date=date,
                    time=time,
                    available=False)
        query = '''
            query testDailyInputView($kitNumber: Int, $date: String) {
                dailyInputView(kitNumber: $kitNumber, date: $date) {
                    available
                    time {
                       time
                    }
                }
            }
        '''
        op_name = 'dailyInputView'
        input_dict = {
            'kitNumber': 29,
            'date': date.__str__().replace('-', '.', 2)
        }
        expected = {
            'dailyInputView': [
                {
                    'available': True,
                    'time': {
                        'time': time.__str__()
                    }
                }
            ]
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)

    def test_daily_input_when_player_does_not_exist(self):
        date = datetime.datetime.now()
        query = '''
            query testDailyInputView($kitNumber: Int, $date: String) {
                dailyInputView(kitNumber: $kitNumber, date: $date) {
                    available
                    time {
                       time
                    }
                }
            }
        '''
        op_name = 'dailyInputView'
        input_dict = {
            'kitNumber': 29,
            'date': date.__str__()[0:10].replace('-', '.', 2)
        }
        expected = {
            'dailyInputView': []
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)


class TestDailyOutputView(GraphQLTestCase):
    def test_daily_input_view_empty(self):
        date = datetime.datetime.now()
        query = '''
            query testDailyOutputView($date: String) {
                dailyOutputView(date: $date) {
                    available
                    time {
                       time
                    }
                    player {
                        lastName
                        kitNumber
                    }
                }
            }
        '''
        op_name = 'dailyOutputView'
        input_dict = {
            'date': date.__str__()[0:10].replace('-', '.', 2)
        }
        expected = {
            'dailyOutputView': []
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)

    def test_daily_input_view_two_players(self):
        player1 = mixer.blend('playarena_scheduler.Player',
                              first_name='Franciszek',
                              last_name='Madej',
                              kit_number=29)
        player2 = mixer.blend('playarena_scheduler.Player',
                              first_name='Jan',
                              last_name='Kowalski',
                              kit_number=10)
        date = mixer.blend('playarena_scheduler.Date',
                           date=datetime.datetime.now().date)
        time = mixer.blend('playarena_scheduler.Time',
                           date=datetime.datetime.now().time)
        mixer.blend('playarena_scheduler.Availability',
                    player=player1,
                    date=date,
                    time=time,
                    available=True)
        mixer.blend('playarena_scheduler.Availability',
                    player=player2,
                    date=date,
                    time=time,
                    available=False)
        query = '''
            query testDailyOutputView($date: String) {
                dailyOutputView(date: $date) {
                    available
                    time {
                       time
                    }
                    player {
                        lastName
                        kitNumber
                    }
                }
            }
        '''
        op_name = 'dailyOutputView'
        input_dict = {
            'date': date.__str__()[0:10].replace('-', '.', 2)
        }
        expected = {
            'dailyOutputView': [
                {
                    'available': True,
                    'time': {
                        'time': time.__str__()
                    },
                    'player': {
                        'lastName': 'Madej',
                        'kitNumber': 29
                    }
                },
                {
                    'available': False,
                    'time': {
                        'time': time.__str__()
                    },
                    'player': {
                        'lastName': 'Kowalski',
                        'kitNumber': 10
                    }
                }
            ]
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)


class TestUserInfo(GraphQLTestCase):
    maxDiff = None

    def test_user_info(self):
        user = User.objects.get(username='graphql')
        test_user = User.objects.create_user(username='test1', password='1')
        test_user_2 = User.objects.create_user(username='test2', password='1')
        captain_perk = mixer.blend('playarena_scheduler.AdditionalPerk',
                                   name='captain')
        vicecaptain_perk = mixer.blend('playarena_scheduler.AdditionalPerk',
                                       name='vicecaptain')
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29,
                    locale='pl',
                    user=user,
                    additional_perks=[captain_perk])
        mixer.blend('playarena_scheduler.Player',
                    first_name='Szymon',
                    last_name='Młodnicki',
                    kit_number=8,
                    locale='pl',
                    user=test_user,
                    additional_perks=[vicecaptain_perk])
        mixer.blend('playarena_scheduler.Player',
                    first_name='Patryk',
                    last_name='Nguyen',
                    kit_number=17,
                    locale='en',
                    user=test_user_2)
        query = '''
            query {
                userInfo {
                    firstName
                    lastName
                    kitNumber
                    isCaptain
                    locale
                }
            }
        '''
        op_name = 'userInfo'
        expected = {
            'userInfo': {
                'firstName': 'Franciszek',
                'lastName': 'Madej',
                'kitNumber': 29,
                'locale': 'PL',
                'isCaptain': True
            }
        }
        response = self.query(query=query, op_name=op_name)
        self.assertResponseNoErrors(response, expected)
        self._client.logout()
        self._client.login(username='test1', password='1')
        expected = {
            'userInfo': {
                'firstName': 'Szymon',
                'lastName': 'Młodnicki',
                'kitNumber': 8,
                'locale': 'PL',
                'isCaptain': True
            }
        }
        response = self.query(query=query, op_name=op_name)
        self.assertResponseNoErrors(response, expected)
        self._client.logout()
        self._client.login(username='test2', password='1')
        expected = {
            'userInfo': {
                'firstName': 'Patryk',
                'lastName': 'Nguyen',
                'kitNumber': 17,
                'locale': 'EN',
                'isCaptain': False
            }
        }
        response = self.query(query=query, op_name=op_name)
        self.assertResponseNoErrors(response, expected)


class TestTeamInfo(GraphQLTestCase):
    def test_team_info(self):
        user = User.objects.get(username='graphql')
        team = mixer.blend('playarena_scheduler.Team',
                           name='KS Styropian',
                           start_hour='08:30:00',
                           end_hour='20:30:00',
                           interval='01:30:00',
                           days_in_week_view=7)
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29,
                    user=user,
                    team=team)
        query = '''
            query {
                teamInfo {
                    name
                    startHour
                    endHour
                    interval
                    daysInWeekView
                }
            }
        '''
        op_name = 'userInfo'
        expected = {
            'teamInfo': {
                'name': 'KS Styropian',
                'startHour': '08:30:00',
                'endHour': '20:30:00',
                'interval': '01:30:00',
                'daysInWeekView': 7
            }
        }
        response = self.query(query=query, op_name=op_name)
        self.assertResponseNoErrors(response, expected)

        query = '''
            query {
                teamInfo {
                    name
                    startHour
                    endHour
                    interval
                    daysInWeekView
                }
            }
        '''
        op_name = 'userInfo'
        expected = {
            'teamInfo': {
                'name': 'KS Styropian',
                'startHour': '08:30:00',
                'endHour': '20:30:00',
                'interval': '01:30:00',
                'daysInWeekView': 7
            }
        }
        response = self.query(query=query, op_name=op_name)
        self.assertResponseNoErrors(response, expected)


class TestAllPlayers(GraphQLTestCase):
    def test_all_players(self):
        team = mixer.blend('playarena_scheduler.Team',
                           name='KS Styropian')
        user = User.objects.get(username='graphql')
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29,
                    team=team,
                    user=user)
        mixer.blend('playarena_scheduler.Player',
                    first_name='Szymon',
                    last_name='Młodnicki',
                    kit_number=8,
                    team=team)
        mixer.blend('playarena_scheduler.Player',
                    first_name='Patryk',
                    last_name='Nguyen',
                    kit_number=17,
                    team=team)
        query = '''
            query allPlayers {
                allPlayers {
                    firstName
                    lastName
                    kitNumber
                }
            }
        '''
        op_name = 'allPlayers'
        input_dict = {
        }
        expected = {
            'allPlayers': [
                {
                    'firstName': 'Franciszek',
                    'lastName': 'Madej',
                    'kitNumber': 29
                },
                {
                    'firstName': 'Szymon',
                    'lastName': 'Młodnicki',
                    'kitNumber': 8
                },
                {
                    'firstName': 'Patryk',
                    'lastName': 'Nguyen',
                    'kitNumber': 17
                }
            ]
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response=response, expected=expected)
