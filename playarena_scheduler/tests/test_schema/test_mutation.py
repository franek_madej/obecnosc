import datetime

from mixer.backend.django import mixer
from django.contrib.auth.models import User

from ...models import Date, Time
from .helper import GraphQLTestCase


class TestCreateAvailability(GraphQLTestCase):
    def test_create_availability(self):
        date = Date(date=datetime.datetime.now().date())
        date.save()
        time = Time(time=datetime.datetime.now().time())
        time.save()
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        query = '''
        mutation ($playerKitNumber: Int, $date: String, $time: String,
                  $available: Boolean) {
            createAvailability (playerKitNumber: $playerKitNumber,
                                time: $time, date: $date,
                                available: $available) {
                success
            }
        }
        '''
        op_name = 'createAvailability'
        input_dict = {
            'playerKitNumber': 29,
            'date': date.__str__().replace('-', '.', 2),
            'time': time.__str__(),
            'available': True
        }
        expected = {
            'createAvailability': {
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

    def test_create_availability_when_date_does_not_exist(self):
        date = datetime.datetime.now().__str__()
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        query = '''
        mutation ($playerKitNumber: Int, $date: String, $time: String,
                  $available: Boolean) {
            createAvailability (playerKitNumber: $playerKitNumber,
                                time: $time, date: $date,
                                available: $available) {
                success
            }
        }
        '''
        op_name = 'createAvailability'
        input_dict = {
            'playerKitNumber': 29,
            'date': date[0:10].replace('-', '.', 2),
            'time': date[11:],
            'available': True
        }
        expected = {
            'createAvailability': {
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

    def test_edit_availability(self):
        date = datetime.datetime.now().__str__()
        mixer.blend('playarena_scheduler.Player',
                    first_name='Franciszek',
                    last_name='Madej',
                    kit_number=29)
        query = '''
        mutation ($playerKitNumber: Int, $date: String, $time: String,
                  $available: Boolean) {
            createAvailability (playerKitNumber: $playerKitNumber,
                                time: $time, date: $date,
                                available: $available) {
                availability {
                    available
                }
                success
            }
        }
        '''
        op_name = 'createAvailability'
        input_dict = {
            'playerKitNumber': 29,
            'date': date[0:10].replace('-', '.', 2),
            'time': date[11:],
            'available': True
        }
        expected = {
            'createAvailability': {
                'availability': {
                    'available': True
                },
                'success': True
            }
        }

        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

        # now check if you can edit it
        input_dict['available'] = False
        expected = {
            'createAvailability': {
                'availability': {
                    'available': False
                },
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)


class TestChangePlayer(GraphQLTestCase):
    def test_change_player(self):
        '''
        Test changing player when team does not exist
        '''

        query = '''
        mutation ($playerKitNumber: Int, $firstName: String, $lastName: String,
                  $position: String, $language: String, $password: String,
                  $team: String) {
            changePlayer (playerKitNumber: $playerKitNumber,
                                firstName: $firstName, lastName: $lastName,
                                position: $position, language: $language,
                                password: $password,
                                team: $team) {
                success
                player {
                    firstName
                    lastName
                }
            }
        }
        '''
        op_name = 'changePlayer'
        input_dict = {
            'playerKitNumber': 29,
            'firstName': 'Stanisław',
            'lastName': 'Madej',
            'position': 'Forward',
            'language': 'pl',
            'password': 'hunter12',
            'team': 'KS Styropian'
        }
        expected = {
            'changePlayer': {
                'success': False,
                'player': None
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

        '''
        Test changing player when team does exist, but position does not
        '''
        team = mixer.blend('playarena_scheduler.Team',
                           name='KS Styropian')

        query = '''
        mutation ($playerKitNumber: Int, $firstName: String, $lastName: String,
                  $position: String, $language: String, $password: String,
                  $team: String) {
            changePlayer (playerKitNumber: $playerKitNumber,
                                firstName: $firstName, lastName: $lastName,
                                position: $position, language: $language,
                                password: $password,
                                team: $team) {
                success
                player {
                    firstName
                    lastName
                }
            }
        }
        '''
        op_name = 'changePlayer'
        input_dict = {
            'playerKitNumber': 29,
            'firstName': 'Stanisław',
            'lastName': 'Madej',
            'position': 'Forward',
            'language': 'pl',
            'password': 'hunter12',
            'team': 'KS Styropian'
        }
        expected = {
            'changePlayer': {
                'success': False,
                'player': None
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

        forward = mixer.blend('playarena_scheduler.Position',
                              name='Forward')
        player = mixer.blend('playarena_scheduler.Player',
                             first_name='Franciszek',
                             last_name='Madej',
                             kit_number='29',
                             position=forward,
                             team=team)
        player.save()
        query = '''
        mutation ($playerKitNumber: Int, $firstName: String, $lastName: String,
                  $position: String, $language: String, $password: String,
                  $team: String) {
            changePlayer (playerKitNumber: $playerKitNumber,
                                firstName: $firstName, lastName: $lastName,
                                position: $position, language: $language,
                                password: $password,
                                team: $team) {
                success
                player {
                    firstName
                    lastName
                }
            }
        }
        '''
        op_name = 'changePlayer'
        input_dict = {
            'playerKitNumber': 29,
            'firstName': 'Stanisław',
            'lastName': 'Madej',
            'position': 'Forward',
            'language': 'pl',
            'password': 'hunter12',
            'team': 'KS Styropian'
        }
        expected = {
            'changePlayer': {
                'success': True,
                'player': {
                    'firstName': 'Stanisław',
                    'lastName': 'Madej'
                    }
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)

    def test_create_player(self):
        forward = mixer.blend('playarena_scheduler.Position',
                              name='Forward')
        forward.save()
        team = mixer.blend('playarena_scheduler.Team',
                           name='KS Styropian')
        team.save()
        query = '''
        mutation ($playerKitNumber: Int, $firstName: String, $lastName: String,
                  $position: String, $language: String, $password: String,
                  $team: String) {
            changePlayer (playerKitNumber: $playerKitNumber,
                                firstName: $firstName, lastName: $lastName,
                                position: $position, language: $language,
                                password: $password,
                                team: $team) {
                success
                player {
                    firstName
                    lastName
                }
            }
        }
        '''
        op_name = 'changePlayer'
        input_dict = {
            'playerKitNumber': 29,
            'firstName': 'Stanisław',
            'lastName': 'Madej',
            'position': 'Forward',
            'language': 'pl',
            'password': 'hunter12',
            'team': 'KS Styropian'
        }
        expected = {
            'changePlayer': {
                'success': True,
                'player': {
                    'firstName': 'Stanisław',
                    'lastName': 'Madej'
                    }
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)


class TestChangeLocale(GraphQLTestCase):
    def test_change_locale(self):
        team = mixer.blend('playarena_scheduler.Team',
                           name='KS Styropian')
        forward = mixer.blend('playarena_scheduler.Position',
                              name='Forward')
        player = mixer.blend('playarena_scheduler.Player',
                             first_name='Franciszek',
                             last_name='Madej',
                             kit_number='29',
                             position=forward,
                             locale='pl',
                             team=team)
        player.save()
        query = '''
        mutation ($playerKitNumber: Int,
                  $locale: String) {
            changeLocale (playerKitNumber: $playerKitNumber,
                                locale: $locale) {
                success
                player {
                    kitNumber
                    locale
                }
            }
        }
        '''
        op_name = 'changeLocale'
        input_dict = {
            'playerKitNumber': 29,
            'locale': 'en'
        }
        expected = {
            'changeLocale': {
                'success': True,
                'player': {
                    'locale': 'EN',
                    'kitNumber': 29
                }
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)


class TestChangePassword(GraphQLTestCase):
    def test_change_password(self):
        user = mixer.blend(User, password='123')
        team = mixer.blend('playarena_scheduler.Team',
                           name='KS Styropian')
        forward = mixer.blend('playarena_scheduler.Position',
                              name='Forward')
        player = mixer.blend('playarena_scheduler.Player',
                             first_name='Franciszek',
                             last_name='Madej',
                             kit_number='29',
                             position=forward,
                             locale='pl',
                             team=team,
                             user=user)
        player.save()
        query = '''
        mutation ($playerKitNumber: Int,
                  $passwordNew: String) {
            changePassword (playerKitNumber: $playerKitNumber,
                                passwordNew: $passwordNew) {
                success
            }
        }
        '''
        op_name = 'changePassword'
        input_dict = {
            'playerKitNumber': 29,
            'password': 'hunter12'
        }
        expected = {
            'changePassword': {
                'success': True,
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)


class TestChangeHours(GraphQLTestCase):
    def test_change_hours(self):
        mixer.blend('playarena_scheduler.Team',
                    name='KS Styropian',
                    start_hour='8:30',
                    end_hour='20:30',
                    interval='1:30',
                    days_in_week_view=8)
        query = '''
        mutation ($teamName: String,
                  $startHour: String,
                  $endHour: String,
                  $interval: String,
                  $daysInWeekView: Int) {
            changeHours (teamName: $teamName, startHour: $startHour,
                         endHour: $endHour, interval: $interval,
                         daysInWeekView: $daysInWeekView) {
                success
            }
        }
        '''
        op_name = 'changeHours'
        input_dict = {
                'teamName': 'KS Styropian',
                'startHour': '11:00',
                'endHour': '19:00',
                'interval': '1:00',
                'daysInWeekView': 10
        }
        expected = {
            'changeHours': {
                'success': True
            }
        }
        response = self.query(query=query, op_name=op_name,
                              input_dict=input_dict)
        self.assertResponseNoErrors(response, expected)
