import json

from django.contrib.auth.models import User
from django.test import Client, TestCase


class GraphQLTestCase(TestCase):

    def setUp(self):
        User.objects.create_user(username='graphql', password='gql')
        self._client = Client()
        self._client.login(username='graphql', password='gql')

    def query(self, query: str, op_name: str = None, input_dict: dict = None):
        '''
        Args:
            query - GraphQL query to run
            op_name - If query is a mutation or named query,
                      you must supply op_name
            input_dict - If provided, GQL $input is set to this
        '''
        body = {'query': query}
        if op_name:
            body['operation_name'] = op_name
        if input_dict:
            body['variables'] = input_dict

        response = self._client.post('/api', json.dumps(body),
                                     content_type='application/json')
        json_response = json.loads(response.content.decode())
        return json_response

    def assertResponseNoErrors(self, response: dict, expected: dict):
        '''
        Assert that the response (as returned from query) has the data
        from expected
        '''
        self.assertNotIn('errors', response, 'Response had no errors')
        self.assertEqual(response['data'], expected, ('Response had correct '
                                                      'data'))
