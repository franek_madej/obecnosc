from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth.models import User


class Position(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class AdditionalPerk(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Team(models.Model):
    name = models.CharField(max_length=200)
    # emblem = models.ImageField(blank=True)
    # maybe one day I will add emblem's functionality
    # currently disabled for sake of testing
    interval = models.TimeField(default='1:30')
    start_hour = models.TimeField(default='8:30')
    end_hour = models.TimeField(default='20:30')
    days_in_week_view = models.IntegerField(default=7)
    show_next_week = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Player(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    kit_number = models.PositiveIntegerField(validators=(
                                                MinValueValidator(1),
                                                MaxValueValidator(99)
                                            ), unique=True)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    additional_perks = models.ManyToManyField(AdditionalPerk, blank=True)
    locale = models.CharField(max_length=2, choices=(
                                        ('en', 'English'),
                                        ('pl', 'Polski'),
                                        ), default='en')
    user = models.OneToOneField(User)

    def is_captain(self):
        if self.additional_perks.filter(name="captain"):
            return True

        elif self.additional_perks.filter(name="vicecaptain"):
            return True
        else:
            return False

    def __str__(self):
        return (str(self.kit_number) + ' - ' + self.first_name[0] + '. '
                + self.last_name)


class Date(models.Model):
    date = models.DateField()

    def __str__(self):
        return str(self.date)

    def save(self, *args, **kwargs):
        if self.pk is None:
            try:
                old_date = Date.objects.get(date=self.date)
                self.pk = old_date.pk
            except Date.DoesNotExist:
                pass
        super(Date, self).save(*args, **kwargs)


class Time(models.Model):
    time = models.TimeField()

    def __str__(self):
        return str(self.time)

    def save(self, *args, **kwargs):
        if self.pk is None:
            try:
                combination = Time.objects.get(time=self.time)
                self.pk = combination.pk
            except Time.DoesNotExist:
                pass
        super(Time, self).save(*args, **kwargs)


class Availability(models.Model):
    date = models.ForeignKey(Date, on_delete=models.CASCADE)
    time = models.ForeignKey(Time, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    available = models.BooleanField(default=True)

    def __str__(self):
        word = ' isn\'t on '
        if self.available:
            word = ' is on '
        return (self.player.__str__() + word + self.date.__str__()
                + ' ' + self.time.__str__())
