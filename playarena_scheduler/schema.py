from datetime import datetime

from django.contrib.auth.models import User

import graphene
from graphene_django.types import DjangoObjectType
from playarena_scheduler.models import (AdditionalPerk, Availability, Date,
                                        Player, Position, Team, Time)


class PlayerType(DjangoObjectType):
    is_captain = graphene.Boolean(source='is_captain')

    class Meta:
        model = Player


class PositionType(DjangoObjectType):
    class Meta:
        model = Position


class TeamType(DjangoObjectType):
    class Meta:
        model = Team


class AvailabilityType(DjangoObjectType):
    class Meta:
        model = Availability


class DateType(DjangoObjectType):
    class Meta:
        model = Date


class TimeType(DjangoObjectType):
    class Meta:
        model = Time


class AdditionalPerkType(DjangoObjectType):
    class Meta:
        model = AdditionalPerk


class ChangeLocale(graphene.Mutation):
    class Input:
        player_kit_number = graphene.Int()
        locale = graphene.String()

    success = graphene.Boolean()
    player = graphene.Field(lambda: PlayerType)

    @staticmethod
    def mutate(root, args, context, info):
        player = Player.objects.get(kit_number=args.get('player_kit_number'))
        player.locale = args.get('locale')
        player.save()

        success = True
        return ChangeLocale(success=success, player=player)


class ChangePassword(graphene.Mutation):
    class Input:
        player_kit_number = graphene.Int()
        password_new = graphene.String()

    success = graphene.Boolean()

    @staticmethod
    def mutate(root, args, context, info):
        player = Player.objects.get(kit_number=args.get('player_kit_number'))
        user = User.objects.get(player=player)
        user.set_password(args.get('password_new'))
        user.save()

        success = True
        return ChangePassword(success=success)


class ChangeHours(graphene.Mutation):
    class Input:
        team_name = graphene.String()
        start_hour = graphene.String()
        end_hour = graphene.String()
        interval = graphene.String()
        days_in_week_view = graphene.Int()

    success = graphene.Boolean()

    @staticmethod
    def mutate(root, args, context, info):
        team = Team.objects.get(name=args.get('team_name'))
        team.start_hour = args.get('start_hour')
        team.end_hour = args.get('end_hour')
        team.interval = args.get('interval')
        team.days_in_week_view = args.get('days_in_week_view')
        team.save()
        success = True

        return ChangeHours(success=success)


class ChangePlayer(graphene.Mutation):
    class Input:
        player_kit_number = graphene.Int()
        first_name = graphene.String()
        last_name = graphene.String()
        position = graphene.String()
        language = graphene.String()
        password = graphene.String()
        team = graphene.String()

    player = graphene.Field(lambda: PlayerType)
    success = graphene.Boolean()

    @staticmethod
    def mutate(root, args, context, info):
        try:
            team = Team.objects.get(name=args.get('team'))
        except Team.DoesNotExist:
            return ChangePlayer(success=False, player=None)

        try:
            position = Position.objects.get(name=args.get('position'))
        except Position.DoesNotExist:
            success = False
            return ChangePlayer(success=success, player=None)

        try:
            player = Player.objects.get(
                    kit_number=args.get('player_kit_number')
                    )
            player.first_name = args.get('first_name')
            player.last_name = args.get('last_name')
            player.locale = args.get('language')
            position = Position.objects.get(name=args.get('position'))
            player.position = position
            player.team = team
            player.save()

        except:
            username = args.get('first_name')[0] + args.get('last_name')
            user = User(username=username, password=args.get('password'))
            user.save()
            player = Player(first_name=args.get('first_name'),
                            last_name=args.get('last_name'),
                            kit_number=args.get('player_kit_number'),
                            position=position,
                            locale=args.get('language'),
                            user=user,
                            team=team)
            player.save()
        success = True
        return ChangePlayer(success=success, player=player)


class CreateAvailability(graphene.Mutation):
    class Input:
        player_kit_number = graphene.Int()
        date = graphene.String()
        time = graphene.String()
        available = graphene.Boolean()

    success = graphene.Boolean()
    availability = graphene.Field(lambda: AvailabilityType)

    @staticmethod
    def mutate(root, args, context, info):
        date = datetime.strptime(args.get('date'), '%Y.%m.%d')
        try:
            date_obj = Date.objects.get(date=date)
        except Date.DoesNotExist:
            date_obj = Date(date=date)
            date_obj.save()

        try:
            time_obj = Time.objects.get(time=args.get('time'))
        except Time.DoesNotExist:
            time_obj = Time(time=args.get('time'))
            time_obj.save()

        player = Player.objects.get(kit_number=args.get('player_kit_number'))
        available = args.get('available')

        try:
            availability_obj = Availability.objects.get(date=date_obj,
                                                        time=time_obj,
                                                        player=player)
            availability_obj.available = available
        except Availability.DoesNotExist:
            availability_obj = Availability(date=date_obj,
                                            time=time_obj,
                                            player=player,
                                            available=available)
            availability_obj.available = available
        availability_obj.save()
        success = True
        return CreateAvailability(availability=availability_obj,
                                  success=success)


class MyMutations(graphene.ObjectType):
    create_availability = CreateAvailability.Field()
    change_locale = ChangeLocale.Field()
    change_hours = ChangeHours.Field()
    change_password = ChangePassword.Field()
    change_player = ChangePlayer.Field()


class Query(graphene.AbstractType):
    daily_input_view = graphene.List(AvailabilityType,
                                     kit_number=graphene.Int(),
                                     date=graphene.String())
    daily_output_view = graphene.List(AvailabilityType,
                                      date=graphene.String())
    user_info = graphene.Field(PlayerType)
    team_info = graphene.Field(TeamType)
    all_players = graphene.List(PlayerType)

    def resolve_daily_input_view(self, args, context, info):
        date = datetime.strptime(args.get('date'), '%Y.%m.%d')
        kit_number = args.get('kit_number')
        try:
            player = Player.objects.get(kit_number=kit_number)
        except Player.DoesNotExist:
            return []
        try:
            date_obj = Date.objects.get(date=date)
        except Date.DoesNotExist:
            date_obj = Date(date=date)
            date_obj.save()
        return Availability.objects.filter(date=date_obj,
                                           player=player)

    def resolve_daily_output_view(self, args, context, info):
        date = datetime.strptime(args.get('date'), '%Y.%m.%d')
        try:
            date_obj = Date.objects.get(date=date)
        except Date.DoesNotExist:
            date_obj = Date(date=date)
            date_obj.save()
        return Availability.objects.filter(date=date_obj)

    def resolve_all_players(self, args, context, info):
        return Player.objects.filter(team=context.user.player.team)

    def resolve_user_info(self, args, context, info):
        return context.user.player

    def resolve_team_info(self, args, context, info):
        return context.user.player.team
