import Vue from 'vue'
import App from './App.vue'
import apolloProvider from './apollo.js'
import store from './store/index.js'
import router from './router.js'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'pl'
})

// eslint-disable-next-line no-new
new Vue({
  el: '#app',
  apolloProvider,
  store,
  router,
  i18n,
  render: h => h(App)
})
