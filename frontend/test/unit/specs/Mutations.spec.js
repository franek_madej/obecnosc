import moment from 'moment'
import mutations from '../../../store/mutations.js'

describe('mutations.js', () => {
  it('updatesTeamInfo', () => {
    const state = {
      teamInfo: {
        name: 'Team Name',
        startHour: '08:30:00',
        endHour: '20:30:00',
        interval: '01:30:00',
        emblem: ''
      },
      userInfo: {
        firstName: '',
        lastName: '',
        kitNumber: 1,
        isCaptain: false
      },
      userSettings: {
        selectedDate: moment(),
        locale: 'en'
      }
    }

    const newTeamInfo = {
      name: 'KS Styropian',
      startHour: '10:00:00',
      endHour: '19:00:00',
      interval: '01:00:00',
      emblem: ''
    }
    mutations.updateTeamInfo(state, newTeamInfo)
    expect(state.teamInfo).to.deep.equal(newTeamInfo)

    const newUserInfo = {
      firstName: 'Franciszek',
      lastName: 'Madej',
      kitNumber: 29
    }
    mutations.updateUserInfo(state, newUserInfo)
    expect(state.userInfo).to.deep.equal(newUserInfo)

    const newSelectedDate = moment(new Date(2017, 7, 10))
    mutations.updateSelectedDate(state, newSelectedDate)
    expect(state.userSettings.selectedDate).to.equal(newSelectedDate)

    mutations.updateSelectedDate(state)
    expect(state.userSettings.selectedDate).to.equal(newSelectedDate)

    mutations.updateTeamInfo(state)
    expect(state.teamInfo).to.equal(newTeamInfo)

    mutations.updateUserInfo(state)
    expect(state.userInfo).to.equal(newUserInfo)

    expect(state.userSettings.locale).to.equal('en')
    mutations.updateUserLocale(state, 'pl')
    expect(state.userSettings.locale).to.equal('pl')
  })
})
