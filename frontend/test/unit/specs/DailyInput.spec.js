import DailyInputController from '../../../components/UI/DailyInput/Controller.vue'
import DailyInputView from '../../../components/UI/DailyInput/View.vue'
import apolloProvider from '../apollo-mock.js'
import store from '../../../store/index.js'
import { mount } from 'avoriaz'
import moment from 'moment'

describe('DailyInput/Controller.vue', () => {
  it('it mounts', () => {
    const wrapper = mount(DailyInputController, {apolloProvider, store})
    const testDate = moment(new Date(2017, 6, 15))
    wrapper.setProps({
      selectedDate: testDate
    })
    expect(wrapper.vm.selectedDay).to.equal(testDate.format('ddd, D.MM'))
    expect(wrapper.vm.GraphQLFormattedDate).to.equal(testDate.format('YYYY.MM.DD'))
    expect(wrapper.vm.GraphQLUserKitNumber).to.equal(1)
  })

  it('applies allDayAvailability', () => {
    const wrapper = mount(DailyInputController, {apolloProvider, store})
    const testDate = moment(new Date(2017, 6, 15))
    wrapper.setProps({
      selectedDate: testDate
    })
    wrapper.setData({
      dailyAvailability: [{
        'toString': '08:30',
        'available': true,
        'edited': false
      }, {
        'toString': '10:00',
        'available': false,
        'edited': false
      }, {
        'toString': '11:30',
        'available': false,
        'edited': false
      }, {
        'toString': '13:00',
        'available': false,
        'edited': false
      }, {
        'toString': '14:30',
        'available': false,
        'edited': false
      }, {
        'toString': '16:00',
        'available': false,
        'edited': false
      }, {
        'toString': '17:30',
        'available': false,
        'edited': false
      }, {
        'toString': '19:00',
        'available': false,
        'edited': false
      }, {
        'toString': '20:30',
        'available': false,
        'edited': false
      }]
    })
    wrapper.vm.applyAllDayAvailability()
    expect(wrapper.data().dailyAvailability[0].available).to.equal(true)
  })

  it('resets allDayAvailability', () => {
    const wrapper = mount(DailyInputController, {apolloProvider, store})
    const testDate = moment(new Date(2017, 6, 15))
    wrapper.setProps({
      selectedDate: testDate
    })
    wrapper.setData({
      dailyAvailability: [{
        'toString': '08:30',
        'available': true,
        'edited': false
      }, {
        'toString': '10:00',
        'available': false,
        'edited': false
      }, {
        'toString': '11:30',
        'available': false,
        'edited': false
      }, {
        'toString': '13:00',
        'available': false,
        'edited': false
      }, {
        'toString': '14:30',
        'available': false,
        'edited': false
      }, {
        'toString': '16:00',
        'available': false,
        'edited': false
      }, {
        'toString': '17:30',
        'available': false,
        'edited': false
      }, {
        'toString': '19:00',
        'available': false,
        'edited': false
      }, {
        'toString': '20:30',
        'available': false,
        'edited': false
      }]
    })
    wrapper.vm.resetAllDayAvailability()
    expect(wrapper.data().dailyAvailability[0].available).to.equal(false)
  })

  it('mutates dailyAvailability on server', () => {
    const wrapper = mount(DailyInputController, {apolloProvider, store})
    const testDate = moment(new Date(2017, 6, 15))
    wrapper.setProps({
      selectedDate: testDate
    })
    const dailyAvailability = [{
      'toString': '08:30',
      'available': true,
      'edited': false
    }, {
      'toString': '10:00',
      'available': false,
      'edited': false
    }, {
      'toString': '11:30',
      'available': false,
      'edited': false
    }, {
      'toString': '13:00',
      'available': false,
      'edited': false
    }, {
      'toString': '14:30',
      'available': false,
      'edited': false
    }, {
      'toString': '16:00',
      'available': false,
      'edited': false
    }, {
      'toString': '17:30',
      'available': false,
      'edited': false
    }, {
      'toString': '19:00',
      'available': false,
      'edited': false
    }, {
      'toString': '20:30',
      'available': false,
      'edited': false
    }]
    wrapper.setData({
      dailyAvailability
    })
    wrapper.vm.applyDailyAvailability()
    // it's just a dummy call, as I already test mutations on Python side of
    // things. I'm not entirely sure how to test Apollo just yet.
  })

  it('getLastWeekdaysAvailability', () => {
    const wrapper = mount(DailyInputController, {apolloProvider, store})
    const testDate = moment(new Date(2017, 6, 15))
    wrapper.setProps({
      selectedDate: testDate
    })
    const dailyAvailability = [{
      'toString': '08:30',
      'available': true,
      'edited': false
    }, {
      'toString': '10:00',
      'available': false,
      'edited': false
    }, {
      'toString': '11:30',
      'available': false,
      'edited': false
    }, {
      'toString': '13:00',
      'available': false,
      'edited': false
    }, {
      'toString': '14:30',
      'available': false,
      'edited': false
    }, {
      'toString': '16:00',
      'available': false,
      'edited': false
    }, {
      'toString': '17:30',
      'available': false,
      'edited': false
    }, {
      'toString': '19:00',
      'available': false,
      'edited': false
    }, {
      'toString': '20:30',
      'available': false,
      'edited': false
    }]
    wrapper.setData({
      dailyAvailability
    })
    wrapper.vm.getLastWeekdaysAvailability()
    wrapper.vm.applyDailyAvailability()
    // it's just a dummy call, as I already test mutations on Python side of
    //
  })
})

describe('DailyInput/View.vue', () => {
  it('has valid default props', () => {
    const wrapper = mount(DailyInputView)
    const data = wrapper.vm.$props
    expect(data.date).to.equal(moment().format('ddd, D.MM'), 'has valid date')
    expect(data.hourList).to.equal(undefined, 'has not defined hourList')
    expect(data.edited).to.equal(true, 'has edited set to true')
  })
})
