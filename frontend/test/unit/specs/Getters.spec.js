import moment from 'moment'
import getters from '../../../store/getters.js'

describe('getters.js', () => {
  it('typical store', () => {
    const currentDate = moment()
    const state = {
      teamInfo: {
        name: 'KS Styropian',
        startHour: '08:30:00',
        endHour: '20:30:00',
        interval: '01:30:00',
        emblem: '',
        daysInWeekView: 7
      },
      userInfo: {
        firstName: 'Franciszek',
        lastName: 'Madej',
        kitNumber: 29,
        isCaptain: false
      },
      userSettings: {
        selectedDate: currentDate,
        locale: 'en'
      }
    }
    expect(getters.selectedDate(state)).to.deep.equal(currentDate)
    expect(getters.teamInterval(state)).to.deep.equal({
      hours: 1,
      minutes: 30
    })
    expect(getters.teamHours(state)).to.deep.equal({
      startHour: '08:30:00',
      endHour: '20:30:00'
    })
    expect(getters.teamDetails(state)).to.deep.equal({
      name: 'KS Styropian',
      daysInWeekView: 7
    })
    expect(getters.userName(state)).to.deep.equal({
      firstName: 'Franciszek',
      lastName: 'Madej'
    })
    expect(getters.userNumber(state)).to.equal(29)
    expect(getters.selectedLocale(state)).to.equal('en')
    expect(getters.isCaptain(state)).to.equal(false)
  })
  it('empty store', () => {
    const state = {
      teamInfo: {
        interval: ''
      }
    }
    expect(getters.teamInterval(state)).to.deep.equal({
      hours: 0,
      minutes: 0
    })
  })
})
