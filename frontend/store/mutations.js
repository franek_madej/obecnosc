const mutations = {
  updateTeamInfo: (state, payload) => {
    if (payload) state.teamInfo = payload
  },
  updateUserInfo: (state, payload) => {
    if (payload) state.userInfo = payload
  },
  updateSelectedDate: (state, payload) => {
    if (payload) state.userSettings.selectedDate = payload
  },
  updateUserLocale: (state, payload) => {
    if (payload) state.userSettings.locale = payload
  }
}

export default mutations
