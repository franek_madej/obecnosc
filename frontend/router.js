import Vue from 'vue'
import VueRouter from 'vue-router'

import Daily from './components/Routes/DailyRoute.vue'
import Weekly from './components/Routes/WeeklyRoute.vue'
import Captain from './components/Routes/CaptainRoute.vue'
import Settings from './components/Routes/Settings.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/weekly' },
  { path: '/daily', component: Daily },
  { path: '/weekly', component: Weekly },
  { path: '/captain', component: Captain },
  { path: '/settings', component: Settings }
]

const router = new VueRouter({
  routes,
  linkActiveClass: 'router-active'
})

export default router
