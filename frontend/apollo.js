import Vue from 'vue'
import { ApolloClient, createNetworkInterface } from 'apollo-client'
import VueApollo from 'vue-apollo'
import Cookies from 'cookies-js'

Vue.use(VueApollo)

const networkInterface = createNetworkInterface({
  uri: '/api',
  transportBatching: true,
  opts: {
    credentials: 'same-origin'
  }
})

networkInterface.use([{
  applyMiddleware (req, next) {
    if (!req.options.headers) {
      req.options.headers = {}  // Create the header object if needed.
    }
    req.options.headers['X-CSRFTOKEN'] = Cookies.get('csrftoken')
    next()
  }
}])

const apolloClient = new ApolloClient({
  networkInterface,
  connectToDevTools: true
})

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

export default apolloProvider
