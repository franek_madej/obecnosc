# obecność
[![build status](https://gitlab.com/franek_madej/obecnosc/badges/master/build.svg)](https://gitlab.com/franek_madej/obecnosc/commits/master)
[![coverage report](https://gitlab.com/franek_madej/obecnosc/badges/master/coverage.svg)](https://gitlab.com/franek_madej/obecnosc/commits/master)

Simple Webapp made using Django and VueJS. It allows you to visualize
availability of your eSport or Sport team and plan basing on it. It was made
for [KS Styropian](https://fb.com/ksstyropian) (Football Team in Playarena Kraków).

#### Used libraries / frameworks / components:

* [VueJS](https://vuejs.org/)
* [Django](https://www.djangoproject.com/)
* [VueApollo](https://github.com/Akryum/vue-apollo) and [Apollo Client](http://www.apollodata.com/)
* [Graphene](http://graphene-python.org/) and [Graphene-Django](https://github.com/graphql-python/graphene-django)
* [Spectre CSS](https://picturepan2.github.io/spectre/)
* [vue-flatpickr-component](https://github.com/ankurk91/vue-flatpickr-component/blob/master/README.md)
* more listed in the `requirements/{base,local,production,test}.txt` and
`package.json`

## Screenshots of v0.2 (beta release):

* Login Screen:
![login screen](./screenshots/login_screen.png)

* Daily View:
![daily view](./screenshots/daily_view.png)

* Weekly View:
![weekly view](./screenshots/weekly_view.png)

* Captain View:
![captain view](./screenshots/captain_view.png)

* Settings View:
![settings view](./screenshots/settings_view.png)

### Features (as of v0.2-beta)

* Cloning availability from previous week
* Polish and English localization
* Captain's View, with number of players that filled availability for the day, best and average hour
* Ability to change Team Settings from Settings route (if member is a captain)
* Ability to change password and locale in Settings route
* Ability to add your Availability in Weekly or Daily View
* Daily View allows you to view per hour Players' availability



### Development features (based on [hello-vue + Django by rokups](https://github.com/rokups/hello-vue-django))

* Django backend in `./backend`
* vuejs (v2) frontend in `./frontend`
* Hot-reload with vue-loader
* eslint linter integration
* Makefile to make your life easy


### Development environment setup

These steps will install all required dependencies including development ones,
 run migrations and start dev server.

```bash
make dev
make migrate
make run
```

### Deployment

These steps will install productio dependencies and build vuejs application to
 `static/dist` folder.

```bash
make prod
make build
```
