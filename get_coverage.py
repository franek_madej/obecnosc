import re

p = open('python_cov', 'r')
python_cov = re.search(r'TOTAL *\w+ *\w+ *([0-9]+)', p.read()).group(1)
p.close()

j = open('js_cov', 'r')
js_cov = re.search(r'Statements *: ([0-9]+.??[0-9]+)', j.read()).group(1)
j.close()

result = (float(python_cov) + float(js_cov)) / 200 * 100

print('cov: ' + str(result) + '%')
